import java.lang.Thread.sleep

group = "io.eroshenkoam"
version = version

plugins {
    java
    maven
    id("io.qameta.allure") version "2.9.6"
}

allure {
    adapter {
        autoconfigure.set(true)
        aspectjWeaver.set(true)

        frameworks {
            junit5{
                adapterVersion.set("2.16.1")
            }
        }
    }
}

tasks.withType(JavaCompile::class) {
    sourceCompatibility = "${JavaVersion.VERSION_11}"
    targetCompatibility = "${JavaVersion.VERSION_11}"
    options.encoding = "UTF-8"
}

tasks.test {
    ignoreFailures = true
    useJUnitPlatform {
        includeEngines("junit-jupiter")
    }
    systemProperty("junit.jupiter.execution.parallel.enabled", "true")
    systemProperty("junit.jupiter.execution.parallel.config.strategy", "dynamic")

    systemProperty("junit.jupiter.extensions.autodetection.enabled", "true")
    doLast {
        sleep(10 * 1000L)
    }
}


repositories {
    jcenter()
    mavenCentral()
    mavenLocal()
}

dependencies {
    compile("commons-io:commons-io:2.6")
    compile("io.qameta.allure:allure-java-commons:2.16.1")
    compile("org.junit.jupiter:junit-jupiter-api:5.8.1")
    compile("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    compile("org.junit.jupiter:junit-jupiter-params:5.8.1")

    testCompile("io.qameta.allure:allure-junit-platform:2.16.1")
}
